const express = require('express');

const router = express.Router();
const { oneLoginApprovalHandler } = require('../handlers/one-login-approval-handler');

router.get('/', oneLoginApprovalHandler);
module.exports = router;
