const express = require('express');

const router = express.Router();
const { viewFormHandler } = require('../handlers/view-form-handler');

router.get('/', viewFormHandler);
module.exports = router;
