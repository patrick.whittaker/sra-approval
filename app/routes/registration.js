const express = require('express');

const router = express.Router();
const { registrationHandler } = require('../handlers/registration-handler');

router.get('/', registrationHandler);
module.exports = router;
