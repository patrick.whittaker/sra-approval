const express = require('express');

const router = express.Router();
const { dthAccessApprovalHandler } = require('../handlers/dth-access-approval-handler');

router.get('/', dthAccessApprovalHandler);
module.exports = router;
