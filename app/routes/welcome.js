const express = require('express');

const router = express.Router();
const { welcomeHandler } = require('../handlers/welcome-handler');

router.get('/', welcomeHandler);
module.exports = router;
