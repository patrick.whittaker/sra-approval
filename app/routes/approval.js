const express = require('express');

const router = express.Router();
const { approvalHandler } = require('../handlers/approval-handler');

router.get('/', approvalHandler);
module.exports = router;
