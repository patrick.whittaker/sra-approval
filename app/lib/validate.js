/* eslint no-restricted-syntax: 0 */
const { logger } = require('./logger');

const MANDATORY = 'This field is mandatory';
const INVALID_EMAIL = 'Invalid email address format';

// Regular expressions for validation
const slackChannelRegex = /^#[a-z0-9_-]{1,21}$/;
const clientIdRegex = /^[A-Za-z0-9]{1,10}$/;
const urlRegex = /^(http(s)?:\/\/)[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)$/;
const arrayRegex = /^\[\s*("(?:\\.|[^"\\])*"\s*(?:,\s*"(?:\\.|[^"\\])*"\s*)*)?\]$/;
const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

// General validation function
const validateField = (input, rules) => {
  for (const rule of rules) {
    const error = rule(input);
    if (error) return error;
  }
  return null;
};

// Validation rules
const rules = {
  fullName: [
    (input) => (!input ? MANDATORY : null),
  ],

  yourEmailAddress: [
    (input) => (!input ? MANDATORY : null),
    (input) => (!emailRegex.test(input) ? INVALID_EMAIL : null),
    (input) => (!input.endsWith('dwp.gov.uk') ? 'Your email address has to end with dwp.gov.uk' : null),
  ],

  teamEmailAddress: [
    (input) => (!input ? MANDATORY : null),
    (input) => (!emailRegex.test(input) ? INVALID_EMAIL : null),
    (input) => (!input.endsWith('dwp.gov.uk') ? 'Team email address has to end with dwp.gov.uk' : null),
  ],

  lineManagerEmailAddress: [
    (input) => (!input ? MANDATORY : null),
    (input) => (!emailRegex.test(input) ? INVALID_EMAIL : null),
    (input) => (!input.endsWith('dwp.gov.uk') ? 'Line manager email address has to end with dwp.gov.uk' : null),
  ],

  slackChannel: [
    (input) => {
      if (input) {
        if (input.startsWith('#')) {
          return !slackChannelRegex.test(input) ? 'Invalid Slack channel format' : null;
        }
        if (!input.startsWith('https://app.slack.com/client')) {
          return 'Invalid Slack channel URL format';
        }
      }
      return null;
    },
  ],

  dwpServiceName: [
    (input) => (!input ? MANDATORY : null),
  ],

  clientId: [
    (input) => (!input ? MANDATORY : null),
    (input) => (input.length > 10 ? 'You have exceeded the maximum number of 10 characters' : null),
    (input) => (!clientIdRegex.test(input) ? 'Invalid character. Only letters and numbers permitted' : null),
  ],

  clientSecret: [
    (input) => (!input ? MANDATORY : null),
  ],

  redirectUrl: [
    (input) => (!input ? MANDATORY : null),
    (input) => (!urlRegex.test(input) ? 'Not a valid URL' : null),
  ],

  scopes: [
    (input) => (!input ? MANDATORY : null),
    (input) => (!arrayRegex.test(input) ? 'This must be an array of strings using double quotes' : null),
  ],

  claims: [
    (input) => (!input ? MANDATORY : null),
    (input) => (!arrayRegex.test(input) ? 'This must be an array of strings using double quotes' : null),
  ],
};

const hasNonNullFields = (errors) => Object.keys(errors).some((key) => errors[key] !== null);

// Main validation handler
const validateHandler = (body) => {
  logger.info('Entering validate handler');
  const errors = {};

  Object.entries(rules).forEach(([field, validationRules]) => {
    errors[field] = validateField(body[field], validationRules);
  });

  return hasNonNullFields(errors) ? errors : {};
};

module.exports = {
  validateHandler,
};
