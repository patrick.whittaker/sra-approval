const myLogger = require('shefcon-dev-idt-node-logger');
const pkg = require('../../package.json');

const logger = myLogger(pkg);

module.exports = { logger };
