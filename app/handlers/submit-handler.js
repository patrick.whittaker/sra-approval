const { logger } = require('../lib/logger');
const { validateHandler } = require('../lib/validate');

const submitHandler = (req, res) => {
  logger.info('Entering submit handler');
  const errors = validateHandler(req.body);

  // If there are validation errors, re-render the form with error messages
  if (Object.keys(errors).length > 0) {
    res.locals.errors = errors;
    res.render('register.njk', {
      ...req.body,
    });
  }
  return errors;
};

module.exports = { submitHandler };
