const { logger } = require('../lib/logger');
const { URL } = require('url');

const { dthItems } = require('./test-config');

const dthAccessApprovalHandler = (req, res) => {
  logger.info('entering dth-access-approval-handler');
  const urlObj = new URL(req.originalUrl, `http://${req.headers.host}`);
  const page = urlObj.searchParams.get('page');
  const view_start = ((parseInt(page) - 1) * 6);
  const view_end = view_start + 5;

  dthItems.forEach((item, index) => {
    if (index >= view_start && index <= view_end) {
      item.show = 'yes';
    } else {
      item.show = 'no';
    };
  });

  const numberOfPages = Math.ceil(dthItems.length / 6);

  res.render('dth-access-approval.njk', {
    items: dthItems,
    page,
    numberOfPages
  });
};

module.exports = { dthAccessApprovalHandler };
