const { logger } = require('../lib/logger');

const welcomeHandler = (req, res) => {
  res.render('welcome.njk', {
  });
};

module.exports = { welcomeHandler };
