const CA_WEB = {
  fullName: 'John Doe',
  yourEmailAddress: 'john.doe@dwp.gov.uk',
  teamEmailAddress: 'team@dwp.gov.uk',
  lineManagerEmailAddress: 'manager@dwp.gov.uk',
  slackChannel: '#general',
  dwpServiceName: 'Carers Allowance',
  clientId: 'client123',
  clientSecret: 'secret',
  redirectUrl: 'https://example.com/callback',
  scopes: '["scope1","scope2"]',
  claims: '["claim1","claim2"]',
  algorithm: 'ES35',
};

const UCI_WEB = {
  fullName: 'Jane Doe',
  yourEmailAddress: 'jane.doe@dwp.gov.uk',
  teamEmailAddress: 'group@dwp.gov.uk',
  lineManagerEmailAddress: 'boss@dwp.gov.uk',
  slackChannel: '#mySlack',
  dwpServiceName: 'Universal Credit once',
  clientId: 'client123',
  clientSecret: 'secret',
  clientSecretExpiryTime: '27-07-2026',
  redirectUrl: 'https://example.com/callback',
  scopes: '["scope1","scope2"]',
  claims: '["claim1","claim2"]',
  algorithm: 'ES25',
};

dthItems = [
  {
    clientId: 'CA-WEB',
    serviceName: 'Carers Allowance',
    status: 'Pending - new',
  },
  {
    clientId: 'UC1-WEB',
    serviceName: 'Universal Credit once',
    status: 'Pending - new',
  },
  {
    clientId: 'ATW-WEB',
    serviceName: 'Access to work',
    status: 'Pending - update',
  },
  {
    clientId: 'BB-WEB',
    serviceName: 'Bereavement Benefit',
    status: 'Pending - update',
  },
  {
    clientId: 'RMD-WEB',
    serviceName: 'RAMBYO',
    status: 'Authorised',
  },
  {
    clientId: 'PCOC',
    serviceName: 'Pensions change of circs',
    status: 'Authorised',
  },
  {
    clientId: 'DLA-WEB',
    serviceName: 'Disability Living Allowance',
    status: 'Pending - new',
  },
  {
    clientId: 'CSP-WEB',
    serviceName: 'Carer Support Payment',
    status: 'Pending - new',
  }
];

oneLoginItems = [
  {
    clientId: 'CA-WEB',
    serviceName: 'Carers Allowance',
    status: 'Pending OL ',
  },
  {
    clientId: 'UC1-WEB',
    serviceName: 'Universal Credit once',
    status: 'Pending OL',
  },
  {
    clientId: 'ATW-WEB',
    serviceName: 'Access to Work',
    status: 'Pending OL',
  },
  {
    clientId: 'UC-WEB',
    serviceName: 'Universal Credit',
    status: 'Pending OL',
  },
  {
    clientId: 'RMD-WEB',
    serviceName: 'RAMBYO',
    status: 'Authorised',
  },
  {
    clientId: 'PCOC',
    serviceName: 'Pensions change of circs',
    status: 'Authorised',
  },
  {
    clientId: 'DLA-WEB',
    serviceName: 'Disability Living Allowance',
    status: 'Pending OL',
  }
];

module.exports = {
  CA_WEB,
  UCI_WEB,
  dthItems,
  oneLoginItems,
};
