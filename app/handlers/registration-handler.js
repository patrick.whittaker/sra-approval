const { URL } = require('url');
const { logger } = require('../lib/logger');

const registrationHandler = (req, res) => {
  const urlObj = new URL(req.originalUrl, `http://${req.headers.host}`);
  const fullName = urlObj.searchParams.get('fullName');
  const yourEmailAddress = urlObj.searchParams.get('yourEmailAddress');
  const lineManagerEmailAddress = urlObj.searchParams.get('lineManagerEmailAddress');
  logger.info('entering registration handler');

  res.render('register.njk', {
    fullName,
    yourEmailAddress,
    lineManagerEmailAddress,
  });
};

module.exports = { registrationHandler };
