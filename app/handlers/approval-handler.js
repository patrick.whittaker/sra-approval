const { logger } = require('../lib/logger');

const approvalHandler = (req, res) => {
  logger.info('entering approval handler');
  res.render('approval.njk', {});
};

module.exports = { approvalHandler };
