const { URL } = require('url');
const { logger } = require('../lib/logger');
const { CA_WEB, UCI_WEB } = require('./test-config');


const viewFormHandler = (req, res) => {
  logger.info('Entering view form handler');

  const urlObj = new URL(req.originalUrl, `http://${req.headers.host}`);
  const id = urlObj.searchParams.get('id');

  let config;

  if (id === 'CA-WEB') {
    config = CA_WEB;
  } else {
    config = UCI_WEB;
  }

  const {
    fullName,
    yourEmailAddress,
    teamEmailAddress,
    lineManagerEmailAddress,
    slackChannel,
    dwpServiceName,
    clientId,
    clientSecret,
    clientSecretExpiryTime,
    redirectUrl,
    scopes,
    claims,
    algorithm,
  } = config;

  res.render('client-approval-form.njk', {
    fullName,
    yourEmailAddress,
    teamEmailAddress,
    lineManagerEmailAddress,
    slackChannel,
    dwpServiceName,
    clientId,
    clientSecret,
    clientSecretExpiryTime,
    algorithm,
    redirect: redirectUrl,
    scopes,
    claims,
    redirectUrl,
  });
};

module.exports = { viewFormHandler };