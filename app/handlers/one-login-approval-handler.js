const { logger } = require('../lib/logger');
const { URL } = require('url');

const { oneLoginItems } = require('./test-config');

const oneLoginApprovalHandler = (req, res) => {
  const urlObj = new URL(req.originalUrl, `http://${req.headers.host}`);
  const page = urlObj.searchParams.get('page');
  const view_start = ((parseInt(page) - 1) * 6);
  const view_end = view_start + 5;

  oneLoginItems.forEach((item, index) => {
    if (index >= view_start && index <= view_end) {
      item.show = 'yes';
    } else {
      item.show = 'no';
    };
  });

  const numberOfPages = Math.ceil(oneLoginItems.length / 6);

  res.render('one-login-approval.njk', {
    items: oneLoginItems,
    page,
    numberOfPages
  });
};

module.exports = { oneLoginApprovalHandler };
