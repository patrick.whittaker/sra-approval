/* eslint no-undef: 0 */

const fs = require('fs');
const path = require('path');
const i18next = require('i18next');
const i18nMiddleware = require('i18next-http-middleware');
const FSBackend = require('i18next-fs-backend');

module.exports = (app) => {
  try {
    // Load the list of namespaces (i.e., translation files) available in the 'en' locale
    const namespaces = fs
      .readdirSync(path.resolve(__dirname, '../locales/en'))
      .map((file) => path.parse(file).name);

    i18next
      .use(FSBackend)
      .use(i18nMiddleware.LanguageDetector)
      .init(
        {
          debug: false,
          ns: namespaces,
          backend: {
            loadPath: path.join(__dirname, '/../locales/{{lng}}/{{ns}}.json'),
          },
          initImmediate: false,
          detection: {
            order: ['querystring', 'cookie', 'header'],

            // Keys or params to lookup language from
            lookupQuerystring: 'lng',
            lookupCookie: 'lng',
            lookupHeader: 'accept-language',
            lookupSession: 'lng',

            // Domain for the language cookie
            cookieDomain: process.env.LANG_COOKIE_DOMAIN || undefined,

            // Cache user language in cookies
            caches: ['cookie'],
          },
          preload: ['en', 'cy'], // Preload 'en' and 'cy' locales
          fallbackLng: 'en', // Fallback language if user language not found
        },
        (err) => {
          if (err) {
            // Log the error if i18n initialization fails
            throw new Error(`Error loading i18n content: ${err.message}`);
          }
        },
      );

    // Apply i18next middleware to the app
    app.use(
      i18nMiddleware.handle(i18next, {
        ignoreRoutes: ['/public'], // Ignore i18n for public routes
      }),
    );
  } catch (err) {
    // Catch any error that occurs during initialization and log it
    throw new Error(`Error initialising i18n ${err.message}`);
  }
};
