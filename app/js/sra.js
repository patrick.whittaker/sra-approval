const enableDisabledInputs = () => {
  const inputs = document.querySelectorAll('input:disabled');
  inputs.forEach(input => input.disabled = false);
}

const handleClear = () => {
  const fullName = document.getElementById('fullName').value;
  const yourEmailAddress = document.getElementById('yourEmailAddress').value;
  window.location.href = `/registration?fullName=${encodeURIComponent(fullName)}&yourEmailAddress=${encodeURIComponent(yourEmailAddress)}`;
}

document.addEventListener('DOMContentLoaded', () => {
  const dateInput = document.getElementById('clientSecretExpiryTime');
  const today = new Date();
  const oneYearFromNow = new Date(today);
  oneYearFromNow.setFullYear(today.getFullYear() + 1);
  const twoYearsFromNow = new Date(today);
  twoYearsFromNow.setFullYear(today.getFullYear() + 2);

  dateInput.value = oneYearFromNow.toISOString().split('T')[0];
  dateInput.min = dateInput.value;
  dateInput.max = twoYearsFromNow.toISOString().split('T')[0];
});

const toggleAccordion = (header) => {
  const content = header.nextElementSibling;
  if (content.style.display === 'block') {
    content.style.display = 'none';
    header.textContent = 'Tell me more';
  } else {
    content.style.display = 'block';
    header.textContent = 'Hide';
  }
};
