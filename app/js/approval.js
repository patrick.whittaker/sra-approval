document
    .getElementById('btn-select')
    .addEventListener('click', function () {
        const selectedOption = document.querySelector('input[name="what-do-you-want-to-do?"]:checked');
        if (selectedOption) {
            const value = selectedOption.value;
            if (value === 'dth-access') {
                window.location.href = 'dth-access';
            } else if (value === 'one-login') {
                window.location.href = 'one-login';
            }
        }
    });