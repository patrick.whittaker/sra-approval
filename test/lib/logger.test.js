/* eslint no-undef: 0 */

const myLogger = require('shefcon-dev-idt-node-logger');
// eslint-disable-next-line no-unused-vars
const loggerModule = require('../../app/lib/logger');
const pkg = require('../../package.json');

jest.mock('shefcon-dev-idt-node-logger');

describe('logger.js functionality', () => {
  test('logger initializes with package info', () => {
    expect(myLogger).toHaveBeenCalledWith(pkg);
  });
});
