/* eslint no-undef: 0 */
const { validateHandler } = require('../../app/lib/validate');
const { logger } = require('../../app/lib/logger');

jest.mock('../../app/lib/logger');

function getValidBody() {
  return {
    fullName: 'John Doe',
    yourEmailAddress: 'john.doe@dwp.gov.uk',
    teamEmailAddress: 'team@dwp.gov.uk',
    lineManagerEmailAddress: 'manager@dwp.gov.uk',
    slackChannel: '#general',
    dwpServiceName: 'ServiceName',
    clientId: 'client123',
    clientSecret: 'secret',
    redirectUrl: 'https://example.com/callback',
    scopes: '["scope1","scope2"]',
    claims: '["claim1","claim2"]',
  };
}

describe('Validation Tests', () => {
  beforeEach(() => {
    logger.info.mockClear();
  });

  const runValidationTest = (body, field, expectedError) => {
    const errors = validateHandler(body);
    if (expectedError) {
      expect(errors[field]).toBe(expectedError);
    } else {
      expect(errors).toEqual({});
    }
  };

  it('should validate all fields correctly', () => {
    const body = getValidBody();
    runValidationTest(body, null, null);
  });

  it('should return error for missing full name', () => {
    const body = { ...getValidBody(), fullName: '' };
    runValidationTest(body, 'fullName', 'This field is mandatory');
  });

  it('should return error for invalid your email address', () => {
    const body = { ...getValidBody(), yourEmailAddress: 'invalidemail' };
    runValidationTest(body, 'yourEmailAddress', 'Invalid email address format');
  });

  it('should return error for your email address not ending with dwp.gov.uk', () => {
    const body = { ...getValidBody(), yourEmailAddress: 'john.doe@gmail.com' };
    runValidationTest(body, 'yourEmailAddress', 'Your email address has to end with dwp.gov.uk');
  });

  it('should return error for invalid team email address format', () => {
    const body = { ...getValidBody(), teamEmailAddress: 'invalidemail' };
    runValidationTest(body, 'teamEmailAddress', 'Invalid email address format');
  });

  it('should return error for team email address not ending with dwp.gov.uk', () => {
    const body = { ...getValidBody(), teamEmailAddress: 'team@gmail.com' };
    runValidationTest(body, 'teamEmailAddress', 'Team email address has to end with dwp.gov.uk');
  });

  it('should return error for invalid line manager email address format', () => {
    const body = { ...getValidBody(), lineManagerEmailAddress: 'invalidemail' };
    runValidationTest(body, 'lineManagerEmailAddress', 'Invalid email address format');
  });

  it('should return error for line manager email address not ending with dwp.gov.uk', () => {
    const body = { ...getValidBody(), lineManagerEmailAddress: 'manager@gmail.com' };
    runValidationTest(body, 'lineManagerEmailAddress', 'Line manager email address has to end with dwp.gov.uk');
  });

  it('should return error for invalid Slack channel format', () => {
    const body = { ...getValidBody(), slackChannel: '#invalid-channel-name-that-is-too-long' };
    runValidationTest(body, 'slackChannel', 'Invalid Slack channel format');
  });

  it('should validate all fields correctly with slackchannel as a URL', () => {
    const body = getValidBody();
    body.slackChannel = 'https://app.slack.com/client/xxx';
    runValidationTest(body, null, null);
  });

  it('should return error if slack channel URL in wrong format', () => {
    const body = getValidBody();
    body.slackChannel = 'https://invalid/slack/channel/url';
    runValidationTest(body, 'slackChannel', 'Invalid Slack channel URL format');
  });

  it('should return error for missing DWP service name', () => {
    const body = { ...getValidBody(), dwpServiceName: '' };
    runValidationTest(body, 'dwpServiceName', 'This field is mandatory');
  });

  it('should return error for client ID exceeding 10 characters', () => {
    const body = { ...getValidBody(), clientId: 'clientid12345' };
    runValidationTest(body, 'clientId', 'You have exceeded the maximum number of 10 characters');
  });

  it('should return error for client ID with invalid characters', () => {
    const body = { ...getValidBody(), clientId: 'client_id!' };
    runValidationTest(body, 'clientId', 'Invalid character. Only letters and numbers permitted');
  });

  it('should return error for missing client secret', () => {
    const body = { ...getValidBody(), clientSecret: '' };
    runValidationTest(body, 'clientSecret', 'This field is mandatory');
  });

  it('should return error for invalid redirect URL format', () => {
    const body = { ...getValidBody(), redirectUrl: 'invalid-url' };
    runValidationTest(body, 'redirectUrl', 'Not a valid URL');
  });

  it('should return error for invalid scopes format', () => {
    const body = { ...getValidBody(), scopes: 'invalid-scopes' };
    runValidationTest(body, 'scopes', 'This must be an array of strings using double quotes');
  });

  it('should return error for invalid claims format', () => {
    const body = { ...getValidBody(), claims: 'invalid-claims' };
    runValidationTest(body, 'claims', 'This must be an array of strings using double quotes');
  });
});
