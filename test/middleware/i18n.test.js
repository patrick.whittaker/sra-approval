/* eslint no-undef: 0 */

const fs = require('fs');
const i18next = require('i18next');
const initI18n = require('../../app/middleware/i18n');

jest.mock('fs');
jest.mock('i18next');
jest.mock('i18next-http-middleware');
jest.mock('i18next-fs-backend');

const mockApp = {
  use: jest.fn(),
};

describe('i18n middleware', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should throw an error if i18n initialization fails', () => {
    const errorMessage = 'Initialization error';

    // Mock the filesystem read to ensure namespaces are defined
    fs.readdirSync.mockReturnValue(['common.json', 'home.json']);

    const i18nextMock = {
      use: jest.fn().mockReturnThis(),
      init: jest.fn().mockImplementation((config, callback) => callback(new Error(errorMessage))),
    };

    i18next.use.mockReturnValue(i18nextMock);

    expect(() => initI18n(mockApp)).toThrow(`Error loading i18n content: ${errorMessage}`);
  });

  it('should throw an error if there is an issue reading the namespaces', () => {
    const errorMessage = 'Filesystem error';
    fs.readdirSync.mockImplementation(() => { throw new Error(errorMessage); });

    expect(() => initI18n(mockApp)).toThrow(`Error initialising i18n ${errorMessage}`);
  });
});
