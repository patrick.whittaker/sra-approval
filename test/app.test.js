/* eslint no-undef: 0 */

const request = require('supertest');
const app = require('../app');
const { validateHandler } = require('../app/lib/validate');

jest.mock('../app/lib/validate', () => ({
  validateHandler: jest.fn(),
}));

describe('Server', () => {
  let server;

  afterEach((done) => {
    server.close(done);
  });

  it('should start the server on the specified port', (done) => {
    server = app.listen(3000, () => {
      request(server)
        .get('/')
        .expect(200, done);
    });
  });
});

describe('App', () => {
  describe('GET /', () => {
    it('should respond with a 200 status for the root route', async () => {
      const response = await request(app).get('/');
      expect(response.status).toBe(200);
    });
  });

  describe('POST /submit', () => {
    it('should render form with errors on POST /submit with invalid data', async () => {
      validateHandler.mockReturnValueOnce({ someError: 'Error message' });

      const response = await request(app)
        .post('/submit')
        .send({
          teamEmailAddress: '',
          lineManagerEmailAddress: '',
          slackChannel: '',
          dwpServiceName: '',
          clientId: '',
          clientSecret: '',
          clientSecretExpiryTime: '',
          redirectUrl: '',
          idTokenSignatureAlgorithm: '',
          scopes: '',
          claims: '',
        });

      expect(response.status).toBe(200);
      expect(response.text).toContain('Error message');
    });

    it('should submit form successfully on POST /submit with valid data', async () => {
      validateHandler.mockReturnValueOnce({});

      const response = await request(app)
        .post('/submit')
        .send({
          teamEmailAddress: 'test@example.com',
          lineManagerEmailAddress: 'manager@example.com',
          slackChannel: '#channel',
          dwpServiceName: 'serviceName',
          clientId: 'clientId',
          clientSecret: 'clientSecret',
          clientSecretExpiryTime: '2024-12-31',
          redirectUrl: 'http://example.com',
          idTokenSignatureAlgorithm: 'RS256',
          scopes: 'openid profile email',
          claims: 'claim1 claim2',
        });

      expect(response.status).toBe(200);
      expect(response.text).toBe('Form submitted successfully!');
    });
  });
});
