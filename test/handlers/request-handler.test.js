/* eslint no-undef: 0 */

const { registrationHandler } = require('../../app/handlers/registration-handler');
const { logger } = require('../../app/lib/logger');

jest.mock('url', () => ({
  URL: jest.fn().mockImplementation((url, base) => {
    const urlInstance = new URL(base + url);
    return urlInstance;
  }),
}));

jest.mock('../../app/lib/logger', () => ({
  logger: {
    info: jest.fn(),
  },
}));

describe('registrationHandler', () => {
  let req;
  let res;

  beforeEach(() => {
    req = {
      originalUrl: '/register?fullName=John+Doe&yourEmailAddress=john.doe%40example.com&lineManagerEmailAddress=manager%40example.com',
      headers: {
        host: 'localhost',
      },
    };

    res = {
      render: jest.fn(),
    };
  });

  it('should log entering registration handler', () => {
    registrationHandler(req, res);
    expect(logger.info).toHaveBeenCalledWith('entering registration handler');
  });

  it('should render the register template with the correct data', () => {
    registrationHandler(req, res);

    expect(res.render).toHaveBeenCalledWith('register.njk', {
      fullName: 'John Doe',
      yourEmailAddress: 'john.doe@example.com',
      lineManagerEmailAddress: 'manager@example.com',
    });
  });
});
