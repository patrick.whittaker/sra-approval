/* eslint no-undef: 0 */

const { submitHandler } = require('../../app/handlers/submit-handler');
const { logger } = require('../../app/lib/logger');
const { validateHandler } = require('../../app/lib/validate');

// Mock the dependencies
jest.mock('../../app/lib/logger', () => ({
  logger: {
    info: jest.fn(),
  },
}));

jest.mock('../../app/lib/validate', () => ({
  validateHandler: jest.fn(),
}));

describe('submitHandler', () => {
  let req;
  let res;

  beforeEach(() => {
    req = {
      body: {
        fullName: 'John Doe',
        yourEmailAddress: 'john@example.com',
        teamEmailAddress: 'team@example.com',
        lineManagerEmailAddress: 'manager@example.com',
        slackChannel: 'channel',
        dwpServiceName: 'service',
        clientId: 'clientId',
        clientSecret: 'clientSecret',
        clientSecretExpiryTime: 'expiryTime',
        redirectUrl: 'http://example.com',
        idTokenSignatureAlgorithm: 'RS256',
        scopes: 'scope1 scope2',
        claims: 'claim1 claim2',
      },
    };

    res = {
      locals: {},
      render: jest.fn(),
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should log entering submit handler', () => {
    validateHandler.mockReturnValue({});

    submitHandler(req, res);

    expect(logger.info).toHaveBeenCalledWith('Entering submit handler');
  });

  it('should validate the request body', () => {
    validateHandler.mockReturnValue({});

    submitHandler(req, res);

    expect(validateHandler).toHaveBeenCalledWith(req.body);
  });

  it('should re-render the form with error messages if there are validation errors', () => {
    const errors = { yourEmailAddress: 'Invalid email' };
    validateHandler.mockReturnValue(errors);

    submitHandler(req, res);

    expect(res.locals.errors).toEqual(errors);
    expect(res.render).toHaveBeenCalledWith('register.njk', {
      ...req.body,
    });
  });

  it('should return errors if there are validation errors', () => {
    const errors = { yourEmailAddress: 'Invalid email' };
    validateHandler.mockReturnValue(errors);

    const result = submitHandler(req, res);

    expect(result).toEqual(errors);
  });

  it('should not re-render the form if there are no validation errors', () => {
    validateHandler.mockReturnValue({});

    submitHandler(req, res);

    expect(res.render).not.toHaveBeenCalled();
  });
});
