/* eslint no-undef: 0 */
const { approvalHandler } = require('../../app/handlers/approval-handler');
const { logger } = require('../../app/lib/logger');

// Mock the logger module
jest.mock('../../app/lib/logger', () => ({
  logger: {
    info: jest.fn(),
  },
}));

describe('approvalHandler', () => {
  // Define a mock for res.render
  const renderMock = jest.fn();

  // Reset the mock before each test
  beforeEach(() => {
    renderMock.mockClear();
    logger.info.mockClear();
  });

  it('should log entering approval handler', () => {
    // Set up mocks for request and response
    const req = {};
    const res = { render: renderMock };

    // Call the approvalHandler function
    approvalHandler(req, res);

    // Expect logger.info to have been called
    expect(logger.info).toHaveBeenCalledWith('entering approval handler');
  });

  it('should render the approval template', () => {
    // Set up mocks for request and response
    const req = {};
    const res = { render: renderMock };

    // Call the approvalHandler function
    approvalHandler(req, res);

    // Check that res.render was called with the correct arguments
    expect(renderMock).toHaveBeenCalledWith('approval.njk', {});
  });
});
