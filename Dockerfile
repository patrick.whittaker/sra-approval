FROM registry.gitlab.com/dwp/dynamic-trust-hub-platform/containers/node-base-image:18-alpine3.17
WORKDIR /app
ARG NPM_GITLAB_PR_TOKEN
RUN npm set registry https://nexus.nonprod.dwpcloud.uk/repository/npm/ && \
    npm set @dwp:registry https://gitlab.com/api/v4/packages/npm/ && \
    npm set //gitlab.com/api/v4/packages/npm/:_authToken=${NPM_GITLAB_PR_TOKEN}
COPY --chown=user:user . .
RUN npm install && npm ci && npm dedupe && npm run build && npm prune --production
EXPOSE 3000
USER user
CMD [ "npm", "start" ]
