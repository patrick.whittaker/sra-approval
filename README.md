# Client Management Registration

## Repository
https://gitlab.com/dwp/dynamic-trust-hub/access-management-services/client-management-ui

## Configure
```cp .env.example .env```

## Getting started

```npm install```

```npm start```


For registration:

```http://localhost:3000/```

or

```http://localhost:3000/registration```



For approval:

```http://localhost:3000/approval```



## Prefill
The first two fields - 'Full name' and 'Your email address' would normally be pre-populated. To populate them:

```http://localhost:3000?fullName=Joe Bloggs&yourEmailAddress=joe.bloggs@dwp.gov.uk```

## Prefill with manager email
The line manager email field might also be pre-populated

```http://localhost:3000?fullName=Joe Bloggs&yourEmailAddress=joe.bloggs@dwp.gov.uk&lineManagerEmailAddress=line.manager@dwp.gov.uk```


## Unit tests
```npm test```


## eSlint
```npm run lint```

## eslint with --fix option
```npm run lint-fix```


# The fields.
These are the current requirements for each of the fields on the form as of 13/06/2024. 
'Guidance notes' refers to the text in the 'Tell me more' accordian fields. 


## Full name
Mandatory

Cannot edit this field.	
This will be populated by the SSO session

Guidance notes:
This has been auto populated by your DWP single sign on Details


## Email address
Mandatory

Cannot edit this field.
This will be populated by the SSO session

Guidance notes:
This has been auto populated/ auto filled? by your DWP single sign on Details


## Shared/Team email address
Optional

Must end in 'dwp.gov.uk'.

Error messages;
Your email address has to end with dwp.gov.uk

Guidance notes:
If you use one, tell us what your team email address is. This is so someone else can pick up messages about your registration to the Dynamic Trust Hub Access Manager if you are away.


## Line Manager email address
Mandatory

Might be read only and populated by the SSO.

Must end in 'dwp.gov.uk'.

Error messages:
Your email address has to end with dwp.gov.uk

Guidance notes:
This has been autopopulated/ auto filled? by your DWP single sign on Details. Your line manager will be informed of changes made to registration


## Your Teams’ Slack Channel URL
Optional

dwpdigital.slack.com # is prefix
 
Guidance:
Enter the hashtag (#) or URL for your teams’ slack channel so it can get updates about your registration application.'
             

## DWP service name
Mandatory

Free text

Guidance:
This is the name of your service that you wish to onboard with Dynamic Trust Hub Access Manager service (DTH AM)
 

## Client ID
Mandatory
 
Auto adds -WEB as the suffix
Limit of 10 characters the user can add in.
Only letters or numbers e.g PIP2-WEB
No special characters like %, £ $ ^ etc.

Error messages:
You have exceeded the maximum number of 10 characters.
Invalid character, only letters and numbers permitted.

Guidance:
Please insert an abbreviated name of your service for connectivity. It is restricted to 10 letters. -WEB will be the suffix. for example Access to work service is; ATW-WEB, and the PIP apply service is PIPAPPLY-WEB. 


## Client secret
Mandatory

A client secret is a secret string that the application uses to prove its identity when requesting a token, this can also can be referred to as an application password.


## Client secret expiry time
Mandatory

User picks from a calendar.
Default is today's date + 12 months.
Minimum length = today's date
Maximum length = today's date + 24 months.


## Redirect UR
Mandatory

Guidance notes:
This is the URL that you wish the service to redirect users or DWP services back to after they have been authorised through DTH AM. Usually the DWP service page or the first screen of your service.


## ID Token Signature Algorithm
Mandatory

User picks from a drop-down list


## Scopes
Mandatory
This should be reflected as an array.
e.g. ["openid", "guid"]

## Claims
Mandatory
e.g. ["guid", "email", "phone", "vot"]
