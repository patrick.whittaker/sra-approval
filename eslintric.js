module.exports = {
  extends: ['@dwp/eslint-config-base', 'airbnb-base'],
  env: {
    node: true,
    es2022: true,
  },
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'no-plusplus': 'off',
  },
};
