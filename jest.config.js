// in jest.config.js
module.exports = {
  testTimeout: 30000,
  collectCoverage: true,
  coverageDirectory: 'coverage',
  coverageReporters: [
    'text',
    'lcov',
  ],
  testEnvironment: 'node',
  transformIgnorePatterns: [
    'node_modules/(?!uuid)',
  ],
};
