const express = require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const i18nMiddleware = require('./app/middleware/i18n');
const welcomeRouter = require('./app/routes/welcome');
const dthAccessApprovalRouter = require('./app/routes/dth-access-approval');
const oneLoginApprovalRouter = require('./app/routes/one-login-approval');
const viewFormRouter = require('./app/routes/view-form');

const app = express();

// Configure Nunjucks template engine
nunjucks.configure([
  path.resolve(__dirname, 'app/views'),
  path.resolve(__dirname, 'node_modules/govuk-frontend/dist/govuk/'),
  path.resolve(__dirname, 'node_modules/govuk-frontend/dist/govuk/components'),
], {
  autoescape: true,
  express: app,
});

// Internationalization middleware
i18nMiddleware(app);

// Middleware to parse JSON and urlencoded data
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Serve static files from the root directory
app.use(express.static(path.resolve(__dirname)));

// Define routes
app.use('/', welcomeRouter);
app.use('/welcome', welcomeRouter);
app.use('/dth-access', dthAccessApprovalRouter);
app.use('/one-login', oneLoginApprovalRouter);
app.use('/view-form', viewFormRouter);

module.exports = app;
